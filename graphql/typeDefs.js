const { gql } = require('apollo-server');

module.exports = gql`
  type User {
    id: ID!
    email: String!
    token: String!
    username: String!
    fullname: String!
    createdAt: String!
    photoUrl: String
    role: String!
  }

  input RegisterInput {
    username: String!
    fullname: String!
    password: String!
    confirmPassword: String!
    email: String!
    photoUrl: String!
  }

  type Query {
    getUser(userId: ID!): User
    getUsers: [User]
  }

  type Mutation {
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
    deleteUser(userId: ID!): String!
    updateUser(
      userId: ID!
      fullname: String!
      email: String!
      role: String!
      photoUrl: String!
    ): User!
  }
`;
